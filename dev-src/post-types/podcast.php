<?php

function podcast_init() {
	register_post_type( 'podcast', array(
		'labels'            => array(
			'name'                => __( 'Podcasts', 'eps' ),
			'singular_name'       => __( 'Podcast', 'eps' ),
			'all_items'           => __( 'Podcasts', 'eps' ),
			'new_item'            => __( 'New podcast', 'eps' ),
			'add_new'             => __( 'Add New', 'eps' ),
			'add_new_item'        => __( 'Add New podcast', 'eps' ),
			'edit_item'           => __( 'Edit podcast', 'eps' ),
			'view_item'           => __( 'View podcast', 'eps' ),
			'search_items'        => __( 'Search podcasts', 'eps' ),
			'not_found'           => __( 'No podcasts found', 'eps' ),
			'not_found_in_trash'  => __( 'No podcasts found in trash', 'eps' ),
			'parent_item_colon'   => __( 'Parent podcast', 'eps' ),
			'menu_name'           => __( 'Podcasts', 'eps' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'podcast_init' );

function podcast_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['podcast'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Podcast updated. <a target="_blank" href="%s">View podcast</a>', 'eps'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'eps'),
		3 => __('Custom field deleted.', 'eps'),
		4 => __('Podcast updated.', 'eps'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Podcast restored to revision from %s', 'eps'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Podcast published. <a href="%s">View podcast</a>', 'eps'), esc_url( $permalink ) ),
		7 => __('Podcast saved.', 'eps'),
		8 => sprintf( __('Podcast submitted. <a target="_blank" href="%s">Preview podcast</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Podcast scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview podcast</a>', 'eps'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Podcast draft updated. <a target="_blank" href="%s">Preview podcast</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'podcast_updated_messages' );
