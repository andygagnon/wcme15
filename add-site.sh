#!/bin/bash
# add a site domain address $1.$2 (example.dev) to local apache server for path ~/dev/$1
# use sudo

if [ "$1" == "" ]; then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]; then
    echo "No arguments provided"
    exit 1
fi

#create a directory, add a placeholder file
cd ~/dev
mkdir $1
cp index.php $1

# go to apache
cd /etc/apache2/sites-available

#create new conf
cat > $1.$2.conf <<EOF

<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /home/andy/dev/$1
	ServerName $1.$2
	ServerAlias www.$1.$2

	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined

</VirtualHost>


EOF

cat $1.$2.conf

#add to hosts
#cd /etc
sed -i '$ a\127.0.1.1  '$1.$2  /etc/hosts

cat /etc/hosts

#enable
a2ensite $1.$2.conf
# disable with a2dissite

#restart
service apache2 reload

#add ifconfig eth0 inet IPAddress to Windows 7 /windows/system32/drivers/etc/hosts
#xxx.xxx.xxx.xxx $1.2

cd ~/dev

#change ownership from root to user andy, test this
chown -R andy:andy $1


