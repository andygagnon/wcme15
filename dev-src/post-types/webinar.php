<?php

function webinar_init() {
	register_post_type( 'webinar', array(
		'labels'            => array(
			'name'                => __( 'Webinars', 'eps' ),
			'singular_name'       => __( 'Webinar', 'eps' ),
			'all_items'           => __( 'Webinars', 'eps' ),
			'new_item'            => __( 'New webinar', 'eps' ),
			'add_new'             => __( 'Add New', 'eps' ),
			'add_new_item'        => __( 'Add New webinar', 'eps' ),
			'edit_item'           => __( 'Edit webinar', 'eps' ),
			'view_item'           => __( 'View webinar', 'eps' ),
			'search_items'        => __( 'Search webinars', 'eps' ),
			'not_found'           => __( 'No webinars found', 'eps' ),
			'not_found_in_trash'  => __( 'No webinars found in trash', 'eps' ),
			'parent_item_colon'   => __( 'Parent webinar', 'eps' ),
			'menu_name'           => __( 'Webinars', 'eps' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'webinar_init' );

function webinar_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['webinar'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Webinar updated. <a target="_blank" href="%s">View webinar</a>', 'eps'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'eps'),
		3 => __('Custom field deleted.', 'eps'),
		4 => __('Webinar updated.', 'eps'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Webinar restored to revision from %s', 'eps'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Webinar published. <a href="%s">View webinar</a>', 'eps'), esc_url( $permalink ) ),
		7 => __('Webinar saved.', 'eps'),
		8 => sprintf( __('Webinar submitted. <a target="_blank" href="%s">Preview webinar</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Webinar scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview webinar</a>', 'eps'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Webinar draft updated. <a target="_blank" href="%s">Preview webinar</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'webinar_updated_messages' );
