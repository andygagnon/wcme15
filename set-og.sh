#!/bin/bash
#set owner:group $1 to folder $2
#move to WP dev path ~/dev

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

cd ~/dev
#set owner group e.g. sudo chown -R www-data:www-data project-folder
chown -R $1:$1 $2
