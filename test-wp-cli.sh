#!/bin/bash
#requires wp-cli.yml to be setup
cd ~/dev

#plugin
#wp plugin install the-events-calendar --activate

#wp user meta update Andy show_welcome_panel 1
#wp user meta update Andy metaboxhidden_dashboard a:3:{i:0;s:18:"dashboard_activity";i:1;s:21:"dashboard_quick_press";i:2;s:17:"dashboard_primary";}

#delete all pages
wp post delete $(wp post list --post_type=page --format=ids) --force
wp menu delete menu-1

homeID=$(wp post create --post_type=page --post_title='Home' --post_status=publish --porcelain)
#echo "homeID=$homeID"
aboutID=$(wp post create --post_type=page --post_title='about' --post_status=publish --porcelain)
#blogID=$(wp post create --post_type=page --post_title='Blog' --post_status=publish --porcelain)
HTMLTagsID=$(wp post create dev-src/HTML-Tags.html --post_type=page --post_title='HTMLTags' --post_status=publish --porcelain)
wp post list --post_type=page --fields=ID,post_title

wp menu create 'Menu 1'
wp menu item add-post menu-1 $homeID
wp menu item add-post menu-1 $aboutID
wp menu item add-post menu-1 $HTMLTagsID

#wp post create dev-src/HTML-Tags.html --post_type=page --post_title='HTML Tags' --post_status=publish
#wp post create --post_type=page --post_title='Home' --post_status=publish

wp menu list
wp menu location assign menu-1 primary

wp option update show_on_front page
wp option update page_on_front $homeID
wp option update page_for_posts $blogID
wp user meta update Andy show_admin_bar_front false

exit

#wp post list --post_type=page --field=ID

#Swp menu create 'Menu 1'
#wp menu list

wp post create --post_type=page --post_title='About' \
  --post_status=publish --porcelain | xargs wp menu item add-post menu-1
#wp post list --post_type=page --field=ID
wp post list --post_type=page  --fields=ID,post_name,post_title



#remove admin bar
wp user meta update Andy show_admin_bar_front false

#sidebars
wp widget delete search-2
#wp widget delete recent-posts-2
wp widget delete recent-comments-2
wp widget delete archives-2
wp widget delete categories-2
wp widget delete meta-2


#need option to turn off user Andy 'Show Toolbar when viewing site'


#wp option update page_on_front 3  #need id from home page
#add option data for Dashboard Screen Options

#exit

#build home page, with data

#build pages: import WXR data e.g. HTML Tag test page

#build menu, add home page & test-html-tags, assign and activate to primary menu



