// Gruntfile.js
// for WordPress Theme development
module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    compass: {
      dist: {
        options: {
          sassDir: 'css/sass',
          cssDir: 'css',
          imagesDir: 'images',
          javascriptsDir: 'js',
          fontsDir: 'fonts',
          outputStyle: 'compact',
          relativeAssets: true,
          noLineComments: true
        }
      },
     boot: {
       options: {
         sassDir: 'bower_components/bootstrap-sass-official/assets/stylesheets',
         cssDir: 'css',
         imagesDir: 'images',
         javascriptsDir: 'js',
         fontsDir: 'fonts',
         outputStyle: 'compact',
         relativeAssets: true,
         noLineComments: true
       }
     }

    },
    watch: {
      options: {
        livereload: true
      },
      css1: {
        files: ['css/sass/*.scss', 'style.css'],
        tasks: ['compass:dist']
      },
      css2: {
        files: ['bower_components/bootstrap-sass-official/assets/stylesheets/*.scss', 'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/*.scss', 'js/*.js'],
        tasks: ['compass:boot']
      },
      myphp: {
         files: ['*.php', 'inc/*.php', 'template-parts/*.php', 'js/*.js']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['compass', 'watch']);
};
