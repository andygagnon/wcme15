#!/bin/bash
#link symbol $3 for WP theme $2 in dev folder $1
#move to WP dev path ~/dev

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1

fi

if [ "$3" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

#create short link $3 for theme $2 in project folder $1
ln -s ~/dev/$1/wp-content/themes/$2 ~/$3
