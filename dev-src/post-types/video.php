<?php

function video_init() {
	register_post_type( 'video', array(
		'labels'            => array(
			'name'                => __( 'Videos', 'eps' ),
			'singular_name'       => __( 'Video', 'eps' ),
			'all_items'           => __( 'Videos', 'eps' ),
			'new_item'            => __( 'New video', 'eps' ),
			'add_new'             => __( 'Add New', 'eps' ),
			'add_new_item'        => __( 'Add New video', 'eps' ),
			'edit_item'           => __( 'Edit video', 'eps' ),
			'view_item'           => __( 'View video', 'eps' ),
			'search_items'        => __( 'Search videos', 'eps' ),
			'not_found'           => __( 'No videos found', 'eps' ),
			'not_found_in_trash'  => __( 'No videos found in trash', 'eps' ),
			'parent_item_colon'   => __( 'Parent video', 'eps' ),
			'menu_name'           => __( 'Videos', 'eps' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'video_init' );

function video_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['video'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Video updated. <a target="_blank" href="%s">View video</a>', 'eps'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'eps'),
		3 => __('Custom field deleted.', 'eps'),
		4 => __('Video updated.', 'eps'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Video restored to revision from %s', 'eps'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Video published. <a href="%s">View video</a>', 'eps'), esc_url( $permalink ) ),
		7 => __('Video saved.', 'eps'),
		8 => sprintf( __('Video submitted. <a target="_blank" href="%s">Preview video</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Video scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview video</a>', 'eps'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Video draft updated. <a target="_blank" href="%s">Preview video</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'video_updated_messages' );
