#!/bin/bash
#touch all bootstrap to recompile SASS

if [ "$1" == "" ]
	then
    echo "No project folder provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No theme name/folder provided"
    exit 1
fi

cd ~/dev
cd $1/wp-content/themes/$2

#Rename _bootstrap.scss to bootstrap.scss;
touch bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/*.scss
cd ~/dev

