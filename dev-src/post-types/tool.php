<?php

function tool_init() {
	register_post_type( 'tool', array(
		'labels'            => array(
			'name'                => __( 'Tools', 'eps' ),
			'singular_name'       => __( 'Tool', 'eps' ),
			'all_items'           => __( 'Tools', 'eps' ),
			'new_item'            => __( 'New tool', 'eps' ),
			'add_new'             => __( 'Add New', 'eps' ),
			'add_new_item'        => __( 'Add New tool', 'eps' ),
			'edit_item'           => __( 'Edit tool', 'eps' ),
			'view_item'           => __( 'View tool', 'eps' ),
			'search_items'        => __( 'Search tools', 'eps' ),
			'not_found'           => __( 'No tools found', 'eps' ),
			'not_found_in_trash'  => __( 'No tools found in trash', 'eps' ),
			'parent_item_colon'   => __( 'Parent tool', 'eps' ),
			'menu_name'           => __( 'EPS Tools', 'eps' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor',  'page-attributes' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
add_action( 'init', 'tool_init' );

function tool_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['tool'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Tool updated. <a target="_blank" href="%s">View tool</a>', 'eps'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'eps'),
		3 => __('Custom field deleted.', 'eps'),
		4 => __('Tool updated.', 'eps'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Tool restored to revision from %s', 'eps'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Tool published. <a href="%s">View tool</a>', 'eps'), esc_url( $permalink ) ),
		7 => __('Tool saved.', 'eps'),
		8 => sprintf( __('Tool submitted. <a target="_blank" href="%s">Preview tool</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Tool scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview tool</a>', 'eps'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Tool draft updated. <a target="_blank" href="%s">Preview tool</a>', 'eps'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'tool_updated_messages' );
