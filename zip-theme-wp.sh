#!/bin/bash
#zip up theme $2 for site in folder $1
#requires wp-cli.yml to be setup

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

cd ~/dev
cd $1/wp-content/themes/$2

#zip up theme here, move it
zip $2.zip *.php
zip $2.zip *.css
zip $2.zip *.png
zip $2.zip css/*.css
zip $2.zip fonts/*.css
zip $2.zip images/*
zip $2.zip inc/*
zip $2.zip js/*.js
zip $2.zip languages/*

rm ~/dev/$2.zip
mv $2.zip ~/dev/$2.zip

cd ~/dev
