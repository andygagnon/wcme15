#!/bin/bash
#backup folder $1 using rsync
# use sudo

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

rsync --verbose  --exclude=.git --exclude=node_modules --exclude=.sass-cache -r $1  ~/bu
cd ~/dev
