<?php

class MySQLSession
    {
    var $hostname;
    var $username;
    var $password;

    var $connected;
    var $dbc;
    var $dbname;
    var $table;

    // table building
    var $fp;

    var $queryDebug=0;

    // contstuctor: called with 'new'
    function MySQLSession()
        {
        $this->connected = false;

        $this->hostname='localhost';
        $this->username='root';
        $this->password='cinog';

        $this->fp = 0;
        }

     function Define( $hostname, $username, $password)
        {
        $this->hostname = $hostname;
        $this->username = $username;
        $this->password = $password;
        }

     function Connect()
        {
////echo "<br /> $this->hostname, $this->username, $this->password";
        $this->dbc = mysql_connect($this->hostname, $this->username, $this->password);

        if  ( !$this->dbc)
            {
            echo "Could not connect to MySQL DB.<br />";
            echo $this->hostname;
            echo $this->username;
            echo $this->password;

            die( mysql_error() . "<br />");
            }
        $this->connected = true;
        }

     function Close()
        {
        if ( !mysql_close( $this->dbc))
            {
            echo "Could not close connection to MySQL DB.<br />";
            echo mysql_error() . "<br />";
            exit;
            }
        $this->connected = false;
        }

     function Select( $dbName)
        {
        // select it
//        $select = mysql_select_db( $dbName, $this->dbc);
        $select = mysql_select_db( $dbName);
        if  ( !$select)
            {
            echo "Could not select DB: $dbName .<br />";
            echo "Error: " . mysql_error( $this->dbc) . "<br />";
            return false;
            }
        $this->dbname = $dbName;

        return true;

        }


     // get a single value from a table
     function Get( $table, $getField, $compareField, $value)
        {
        $debug = 0;
        $debug = 1;

        $query = "SELECT $getField FROM '$table' WHERE $compareField = '$value' COLLATE latin1_general_cs";
        $query = "SELECT $getField FROM $table WHERE $compareField = '$value'";
        $result = mysql_query( $query);
        if ( $debug)
            echo "Result for ' " . $query . " ' is: ". $result . ".<br />";

        $str = '';
        // count results
        $numResults = mysql_num_rows( $result);
        if ( $numResults == 0)
            {
            if ( $debug)
                {
                echo "No results found for " . $table . ".<br />";
                $str .= "query:" . $query . "rows: " . $numResults . " result: " . $result;
                }
            return( $str);
            }
        else
            {
            if ( $debug)
                echo $numResults . " results returned for '" . $query . "'.<br />";
            }

        // show results
        for ( $i = 0; $i < $numResults; $i++)
            {
            $row = mysql_fetch_array( $result);
            if ( $debug)
                echo "Row: " . $row[0] . " <br />";
            }

        $str = $row[ 0];

        return ( $str);
        }

     // get a big chunk or column of data, return array
     function GetChunk( $table, $getField, $compareField, $value)
        {
        $debug = 1;
        $debug = 0;

        $query = "SELECT $getField FROM $table WHERE $compareField = '$value'";
        $result = mysql_query( $query);
        if ( $debug)
            echo "Result for ' " . $query . " ' is: ". $result . ".<br />";

        $str = '';
        // count results
        $numResults = mysql_num_rows( $result);

        if ( $numResults == 0)
            {
            if ( $debug)
                {
                echo "No results found for " . $table . ".<br />";
                $str .= "query:" . $query . "rows: " . $numResults . " result: " . $result;
                }
            return( $str);
            }
        else
            {
            if ( $debug)
                echo $numResults . " results returned for '" . $query . "'.<br />";
            }

        $data = array();

        // show results
        for ( $i = 0; $i < $numResults; $i++)
            {
            $row = mysql_fetch_array( $result);
            $data[ $i] = $row[ 0];
            if ( $debug)
                echo "Row: " . $row[0] . " <br />";
            }


        return ( $data);
        }




     // display a table, in table format
     function ShowAll( $table)
        {
        $debug = 0;

        // query
        $query = "SHOW COLUMNS FROM  " . $table;
        $query = "SELECT * FROM  " . $table . " WHERE 1";
//        $result = mysql_query( $query, $this->dbc);
        $result = mysql_query( $query);

        if ( $debug)
            {
            echo "Query: " . $query . "<br />";
            echo "Result: " . $result . "<br />";
            }
        $error = mysql_error();

        if ( $debug)
            {
            if ( $error == '') $error = 'None';
            echo "Error: " . $error . "<br />";
            echo "<br />";
            }

        // count results
        if ( $result)
            $numResults = mysql_num_rows( $result);
        else
            $numResults = 0;
        if ( $numResults == 0)
            {
            echo "No results found for " . $table . ".<br />";
            return;
            exit;
            }
        else if ( $debug)
            echo $numResults . " results returned for '" . $query . "'.<br />";

        // show results
        echo " <table>";
        for ( $i = 0; $i < $numResults; $i++)
            {
            $row = mysql_fetch_row( $result);
            echo "<tr>";
            $count = mysql_num_fields( $result);
            for ( $j = 0; $j < $count; $j++)
                {
                echo "<td>";
//                echo $row[ $j] . " ";
                echo urldecode( $row[ $j]) . " ";
                echo "</td>";
                }
            echo "</tr>";
//            echo " <br />";
            }
        echo " </table>";
        }

    // run a mysql query
     function Query( $qry)
        {
        $debug = $this->queryDebug;

        if ( $qry == '')
            {
            echo "Empty query.<br />";
            return;
            }

        $result = mysql_query( $qry);
        if ( $debug)
            echo "Result for ' " . $qry . " ' is: ". $result . ".<br />";

        $error = mysql_error();
        if ( $debug)
            {
            if ( $error == '') $error = 'None';
            echo "Error: " . $error . "<br />";
            echo "<br />";
            }

        return( $result);
        }

     // fetch a row from a result
     function Fetch( $result)
        {
        $debug = 0;
        $debug = 1;

        return @mysql_fetch_array( $result);
        }

     // Return the number of rows, or size of result
     function Size( $result)
        {
        $debug = 1;
        $debug = 0;

        return( mysql_num_rows( $result));
        }


    ///////////////////////////////////////////////////////////////////
    // DB Building Code

    // Read a file
     function ReadFile( $fileName)
        {

        if (!($fp = fopen($fileName, "r"))) {
           die("could not open input file from $fileName.");
        }
        $data = fread($fp, filesize($fileName));
        fclose($fp);

        return( $data);
        }

    // Parse an XML file into a value array and index array
    // Doesn't work.  Can't pass arrays by reference???
    function ParseXML( $data, &$vals, &$index)
        {
        $debug = 0;
        $show = 0;

        // open parser
        $p = xml_parser_create();
        xml_parser_set_option($p, XML_OPTION_SKIP_WHITE,1);
        xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($p, XML_OPTION_SKIP_TAGSTART, 0);

        // Break XML file into 2 parts, vals and index
        xml_parse_into_struct($p, $data, $vals, $index);
        xml_parser_free($p);

        if ( $debug)
            {
            echo "<pre>";

            if ( isset ( $index[ dbname][0]))
                echo '<br /> dbname: ' . $vals[ $index[ dbname][0]][value];
            if ( isset ( $index[ tablename][0]))
                echo '<br /> tablename: ' . $vals[ $index[ tablename][0]][value];
            if ( isset ( $index[ addid][0]))
                echo '<br /> addid: ' . $vals[ $index[ addid][0]][value];
            if ( isset ( $index[ field][0]))
                $size =  count( $index[ field]);
            for ( $i = 0; $i < $size; $i++)
                echo '<br /> field: ' . $vals[ $index[ field][ $i]][value];
            if ( isset ( $index[ type][0]))
                $size =  count( $index[ type]);
            for ( $i = 0; $i < $size; $i++)
                echo '<br /> type: ' . $vals[ $index[ type][ $i]][value];
            echo "</pre>";
            }

        if ( $show)
            {
            echo "<pre>";
            echo "<br />";
            echo "vals<br />";
           $temp = print_r( $vals);
            echo "<br />";
            echo "index<br />";
            $temp = print_r( $index);
            echo "<br />";
            echo "</pre>";
            }
        }

    function BuildTableDef( $tableName, $names, $types, $span, $addID)
        {
        $str = '';
        $str .= "CREATE TABLE  $tableName (";
        $i = 0;
        while ( $i < ($span - 1))
            {
            $str .= " " . $names[ $i] . " " . $types[ $i] . ",";

            $i++;
            }
        $str .= " " . $names[ $i] . " " . $types[ $i];
        if ( $addID)
            $str .= ", id INT(10) NOT NULL AUTO_INCREMENT, PRIMARY KEY( id)";
//            $str .= ", PRIMARY KEY( id)";

        $str .= ")";

        return( $str);
        }

    // Excel XML file
    function BuildDatabaseFromXML( $xmlFileName, $dbname, $table, $span, $colNames, $colTypes, $debug = 0)
        {
        $show = 0;

        $addID = true;  // add primary key ID
    //    $addID = false;  // add primary key ID

        // get XML data
        $data = $this->ReadFile( $xmlFileName);
        $vals = array();
        $index = array();

if (1)
{
        $this->ParseXML( $data, $vals, $index);
}
else
{
        // open parser
        $p = xml_parser_create();
        xml_parser_set_option($p, XML_OPTION_SKIP_WHITE,1);
        xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($p, XML_OPTION_SKIP_TAGSTART, 0);
        // Break XML file into 2 parts, vals and index
        xml_parse_into_struct($p, $data, $vals, $index);
        xml_parser_free($p);
}
        if ( $show)
            {
            echo "<pre>";
            echo "<br />";
            echo "vals<br />";
            $temp = print_r( $vals);
            echo "<br />";
            echo "index<br />";
            $temp = print_r( $index);
            echo "<br />";
            echo "</pre>";
            }

        // Connect To Database
        $this->Connect();
        if ( $debug)
            echo "Connected to MySQL DB.<br />";

        // create it
        $query = "CREATE DATABASE IF NOT EXISTS $dbname";
        $result = mysql_query( $query, $this->dbc);

        // select it
        $this->Select( $dbname);
        if ( $debug)
            echo "Selected " . $dbname . ".<br />";
        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

        // we're going to rebuild each time, so delete it
        $query = 'DROP TABLE ' . $table;
        $result = mysql_query( $query, $this->dbc);

        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

        // define MySQL table ($span elements)
        $queryTableDefinition = $this->BuildTableDef( $table, $colNames, $colTypes, $span, $addID);

        if ( $debug)
            echo "Def: " . $queryTableDefinition . "<br />";

        // add table
        $result = mysql_query( $queryTableDefinition, $this->dbc);
        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

        // load data into table one row at a time
        $dbData = array();
        // get the data indices
        foreach ( $index['Data'] as $i => $v_index)
            {
            if ( $debug)
                echo "key: " . $i . " value: " . $v_index . "<br />";
            // magic number of span
            if ( $i != 0 && $i % $span == 0)
                {
                // time to put it into the database
                // build query string
                $query = "INSERT INTO $table VALUES ('";
                for ( $j = 0; $j < ($span - 1); $j++)
                    {
                    $query .= $dbData[ $j] . "', '";
                    }
                $query .= $dbData[ $j];
                if ( $addID)
                    $query .= "', '0";
                $query .=  "')";

                $result = mysql_query( $query, $this->dbc);
// debug: see what's inserted
                if ( $debug)
                    {
                    echo mysql_affected_rows( $this->dbc) . " inserted into DB.<br />";
                    echo "Query: " . $query . "<br />";
                    echo "Error: " . mysql_error() . "<br />";
                    }
                }

            // get the data values
            foreach ($vals[ $v_index] as $xml_data => $v_vals )
                {
                if ( $xml_data == 'value')
                    {
                    $v_vals = mysql_real_escape_string( $v_vals);
                    $dbData [ $i % $span] = $v_vals;
                    if ( $debug)
                        echo "Data[ " . $i % $span . "]: " . $dbData [ $i % $span] . "<br />";
                    // clean up strings

                    }
                }
            }

        // build query string for last one
        $query = "INSERT INTO $table VALUES ('";
        for ( $j = 0; $j < ($span - 1); $j++)
            {
            $query .= $dbData[ $j] . "', '";
            }
        $query .= $dbData[ $j];
        if ( $addID)
            $query .= "', '0";
        $query .=  "')";

        $result = mysql_query( $query, $this->dbc);
// debug: see what's inserted
            if ( $debug)
                {
                echo mysql_affected_rows( $this->dbc) . " inserted into DB.<br />";
                echo "Query: " . $query . "<br />";
                echo "Error: " . mysql_error() . "<br />";
                }


        // show the result
        // query
        $query = "SELECT * FROM  " . $table;
        $query = "SHOW COLUMNS FROM  " . $table . ";";
        $query = "SELECT * FROM  $table WHERE 1";
        $result = mysql_query( $query);
        if ( $debug)
            echo "Result for ' " . $query . " ' is: ". $result . ".<br />";

        // count results
        $numResults = mysql_num_rows( $result);
        if ( $numResults == 0)
            {
            if ( $debug)
                echo "No results found for " . $table . ".<br />";
            }
        else
            if ( $debug)
                echo $numResults . " results returned for '" . $query . "'.<br />";

        if ( $debug)
            // show results
            for ( $i = 0; $i < $numResults; $i++)
                {
                $row = mysql_fetch_row( $result);
                echo "<i>start</i>";
                for ( $j = 0; $j < $span; $j++)
                    echo " <br />" . $colNames[ $j] . ":" . $row[ $j];
                echo"<br /><i>end</i><br />";
                }


        $this->Close();
        if ( $debug)
            echo "Closed connection to MySQL DB.<br />";

        }

    // build from tsv values
    function BuildDatabaseFromTSV( $inFileName, $dbname, $table, $span, $colNames, $colTypes, $ignoreHeader = false)
        {
        $show = 1;
        $show = 0;
        $debug = 1;
        $debug = 0;

        $addID = true;  // add primary key ID
//        $addID = false;  // add primary key ID

        // get XML data
//        $data = $this->ReadFile( $inFileName);

        if ( $show)
            {
            echo "<pre>";
            echo "<br />";
            echo "data<br />";
            echo $data . "<br />";
            echo "<br />";
            echo "</pre>";
            }

        // Connect To Database
        $this->Connect();
        if ( $debug)
            echo "Connected to MySQL DB.<br />";

        // create it
        $query = "CREATE DATABASE IF NOT EXISTS $dbname";
        $result = mysql_query( $query, $this->dbc);

        // select it
        $this->Select( $dbname);
        if ( $debug)
            echo "Selected " . $dbname . ".<br />";
        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

        // we're going to rebuild each time, so delete it
        $query = 'DROP TABLE ' . $table;
        $result = mysql_query( $query, $this->dbc);

        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

        // define MySQL table ($span elements)
        $queryTableDefinition = $this->BuildTableDef( $table, $colNames, $colTypes, $span, $addID);

        if ( $debug)
            echo "Def: " . $queryTableDefinition . "<br />";

        // add table
        $result = mysql_query( $queryTableDefinition, $this->dbc);
        if ( $debug)
            {
            $error = mysql_error();
            if ( $error == '')
                $error = 'None';
            echo "Error: " . $error . ".<br />";
            }

//        $query = "LOAD DATA LOCAL INFILE '$inFileName' INTO TABLE $table FIELDS TERMINATED BY '\t'";
        $query = "LOAD DATA LOCAL INFILE '$inFileName' INTO TABLE $table FIELDS TERMINATED BY '\t' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'";
        if ( $ignoreHeader)
            $query .= " IGNORE 1 LINES";

        // load data into table one row at a time
        $result = mysql_query( $query, $this->dbc);
        if ( $debug)
            {
            echo mysql_affected_rows( $this->dbc) . " inserted into DB.<br />";
            echo "Query: " . $query . "<br />";
            echo "Error: " . mysql_error() . "<br />";
            }

        // show the result
        // query
        $query = "SELECT * FROM  $table WHERE 1";
        $result = mysql_query( $query);
        if ( $debug)
            echo "Result for ' " . $query . " ' is: ". $result . ".<br />";

        // count results
        $numResults = mysql_num_rows( $result);
        if ( $numResults == 0)
            {
            if ( $debug)
                echo "No results found for " . $table . ".<br />";
            }
        else
            if ( $debug)
                echo $numResults . " results returned for '" . $query . "'.<br />";

        if ( $debug)
            // show results
            for ( $i = 0; $i < $numResults; $i++)
                {
                $row = mysql_fetch_row( $result);
                echo "<i>start</i>";
                for ( $j = 0; $j < $span; $j++)
                    echo " <br />" . $colNames[ $j] . ":" . $row[ $j];
                echo"<br /><i>end</i><br />";
                }


        $this->Close();
        if ( $debug)
            echo "Closed connection to MySQL DB.<br />";

        }



    }   // end class


?>
