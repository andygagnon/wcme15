#!/bin/bash
#twitter bootstrap to WP sed script
#convert theme $2 for site in project folder $1
#requires wp-cli.yml to be setup?

if [ "$1" == "" ]
	then
    echo "No project folder provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No theme name/folder provided"
    exit 1
fi


#assign variable
project=$1
#convert to upper, lower case
project_upper=$(echo $project | tr  '[:lower:]' '[:upper:]')
project_lower=$(echo $project | tr  '[:upper:]' '[:lower:]')
#assign variable
theme=$2
#convert to upper, lower case
theme_upper=$(echo $theme | tr  '[:lower:]' '[:upper:]')
theme_lower=$(echo $theme | tr  '[:upper:]' '[:lower:]')


cd ~/dev
cd $1/wp-content/themes/$2
#Rename _bootstrap.scss to bootstrap.scss;
mv bower_components/bootstrap-sass-official/assets/stylesheets/_bootstrap.scss bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap.scss

# commment out unnecessary imports
sed -i '26,+11 s_@import_//@import_' bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap.scss
sed -i '44,+2 s_@import_//@import_' bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap.scss

#add import to main.scss
sed -i '1 i @import \"..\/..\/bower_components\/bootstrap-sass-official\/assets\/stylesheets\/bootstrap\/_variables.scss\";' css/sass/main.scss
sed -i '1 i @import \"..\/..\/bower_components\/bootstrap-sass-official\/assets\/stylesheets\/bootstrap\/_mixins.scss\";' css/sass/main.scss

#add some css
sed -i '$ a \
\
a, a:visited {\
	color: $link-color;\
}\
a:hover, a:focus {\
	color: $link-hover-color;\
}\
a:active {\
	color: $link-color;\
}' css/sass/main.scss


#Add media queries to bottom (mobile first css)

sed -i '$ a \
\
//@media only screen and (min-width: $screen-xxs) { // xxs > 160px, phones\
//}\
// tablet break\
@media only screen and (min-width: $screen-xs) { // xs <> 480px, small, phones\
}\
@media only screen and (min-width: $screen-sm) { // small > 768px, tablet, portrait\
}\
// desktop break, ipad landscape break\
@media only screen and (min-width: $screen-md) { // medium > 992px, tablet landscape\
	body { color: blue; }\
}\
@media only screen and (min-width: $screen-lg) { // large, wide > 1100px\
}' css/sass/main.scss

exit
