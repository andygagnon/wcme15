#!/bin/bash
#backup theme in folder $2 for site in wp folder $1 using rsync
# use sudo

if [ "$1" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No arguments provided"
    exit 1
fi

cd ~/dev/$1/wp-content/themes
rsync --verbose  --exclude=node_modules --exclude=.sass-cache --exclude=.git -r $2  ~/bu
cd ~/dev
