#!/bin/bash
#twitter bootstrap to WP sed script
#convert theme $2 for site in project folder $1
#requires wp-cli.yml to be setup?

if [ "$1" == "" ]
	then
    echo "No project folder provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No theme name/folder provided"
    exit 1
fi


#assign variable
project=$1
#convert to upper, lower case
project_upper=$(echo $project | tr  '[:lower:]' '[:upper:]')
project_lower=$(echo $project | tr  '[:upper:]' '[:lower:]')
#assign variable
theme=$2
#convert to upper, lower case
theme_upper=$(echo $theme | tr  '[:lower:]' '[:upper:]')
theme_lower=$(echo $theme | tr  '[:upper:]' '[:lower:]')

#echo $project;echo $project_upper;echo $project_lower;echo $theme;echo $theme_upper;echo $theme_lower

cd ~/dev

#search/replace theme name stub
cd $1/wp-content/themes/$2

#index, page, single, archive
sed -i 's/site-main/site-main col-xs-12 col-md-8/' page.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' index.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' single.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' archive.php

#sidebar
sed -i 's/widget-area/widget-area col-xs-12 col-md-4/' sidebar.php

#footer
sed -i '20 i\
<\div> <!-- end container -->'\
 footer.php
exit
