#!/bin/bash
#Add twitter bootstrap to WP via sed
#convert theme $2 for site in project folder $1

if [ "$1" == "" ]
	then
    echo "No project folder provided"
    exit 1
fi

if [ "$2" == "" ]
	then
    echo "No theme name/folder provided"
    exit 1
fi


#assign variable
project=$1
#convert to upper, lower case
project_upper=$(echo $project | tr  '[:lower:]' '[:upper:]')
project_lower=$(echo $project | tr  '[:upper:]' '[:lower:]')
#assign variable
theme=$2
#convert to upper, lower case
theme_upper=$(echo $theme | tr  '[:lower:]' '[:upper:]')
theme_lower=$(echo $theme | tr  '[:upper:]' '[:lower:]')

#echo $project;echo $project_upper;echo $project_lower;echo $theme;echo $theme_upper;echo $theme_lower

cd ~/dev

#search/replace theme name stub
cd $1/wp-content/themes/$2
sed -i s/Chesterton/"$theme_upper"/ functions.php
sed -i s/chesterton/"$theme_lower"/ functions.php
sed -i s/Chesterton/"$theme_upper"/ inc/custom-theme-options.php
sed -i s/chesterton/"$theme_lower"/ inc/custom-theme-options.php
sed -i s/Chesterton/"$theme_upper"/ inc/util.php
sed -i s/chesterton/"$theme_lower"/ inc/util.php
sed -i s/Chesterton/"$theme_upper"/ inc/widgets.php
sed -i s/chesterton/"$theme_lower"/ inc/widgets.php


#header.php
#remove <nav>
sed -i '30,+3 d' header.php

sed -i '24 i \
	<div class="container">' header.php

#add new <nav>
sed -i '30 i \
	<nav class="navbar navbar-default" role="navigation">\
        <div class="navbar-header">\
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">\
            <span class="sr-only">Toggle navigation</span>\
            <span class="icon-bar"></span>\
            <span class="icon-bar"></span>\
            <span class="icon-bar"></span>\
          </button>\
        </div>\
		<?php\
			$menu_args = array(\
				"theme_location" => "primary",\
				"container_class" => "navbar-collapse collapse",\
				"menu_class" => "nav navbar-nav",\
				"menu_id" => "main-primary-menu",\
				);\
			wp_nav_menu( $menu_args);\
		?>\
	</nav>\
	' header.php
#add container
sed -i '52 i\
 	</div><!-- end container -->\
 	\
 	<div class="container">'\
	 header.php

#add row class
sed -i 's/site-content/site-content row/' header.php


#index, page, single, archive
sed -i 's/site-main/site-main col-xs-12 col-md-8/' page.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' index.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' single.php
sed -i 's/site-main/site-main col-xs-12 col-md-8/' archive.php

#sidebar
sed -i 's/widget-area/widget-area col-xs-12 col-md-4/' sidebar.php

#footer
sed -i '20 i\
<\div> <!-- end container -->'\
 footer.php
exit


