#!/bin/bash
#wp-cli script to build a basic wordpress site, using wp-cli.yml file data

#build wp-cli.local.yml

#get files, build db
wp core download
wp core config
wp db create
wp core install

#create users
wp user create Andy contact@andregagon.com --user_pass='test' --role=administrator
wp user create Bill bill@andregagon.com --user_pass='test' --role=administrator

#remove admin user 1
wp user delete admin --reassign=Andy




